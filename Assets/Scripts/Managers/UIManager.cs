﻿using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
	public Text targetText, scoreText, livesText;

	public int Score { get; set; }

	private void Awake()
	{
		Brick.onBrickDestruction += OnBrickDestruction;
		BricksManager.OnLevelLoaded += OnLevelLoaded;
		GameManager.onLiveLost += OnLiveLost;
	}

	private void Start()
	{
		OnLiveLost(GameManager.Instance.availibleLives);
	}

    private void OnLevelLoaded()
	{
		UpdateRemainingBricksText();
		UpdateScoreText(0);
	}

	private void OnBrickDestruction(Brick brick)
	{
		UpdateRemainingBricksText();
		UpdateScoreText(10);
	}

	private void UpdateScoreText(int increment)
	{
		this.Score += increment;

		string scoreString = this.Score.ToString().PadLeft(5, '0'); 
		scoreText.text = $"SCORE:\n{scoreString}";
	}

	private void OnLiveLost(int remainingLives)
    {
        livesText.text = $"LIVES:\n{remainingLives}";
    }

	private void UpdateRemainingBricksText()
	{
		var remainingBricks =  BricksManager.Instance.RemainingBricks.Count;
		var initialBricks = BricksManager.Instance.InitialBricksCount;

		targetText.text = $"TARGET:\n{remainingBricks} / {initialBricks}";
	}

	private void OnDisable()
	{
		Brick.onBrickDestruction -= OnBrickDestruction;
		BricksManager.OnLevelLoaded -= OnLevelLoaded;
		GameManager.onLiveLost -= OnLiveLost;
	}
}
