﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class BricksManager : MonoBehaviour
{
    
#region Singleton
    
    private static BricksManager _instance;
    public static BricksManager Instance => _instance;
	public static event Action OnLevelLoaded;

    private void Awake()
    {
        if(_instance != null)
        	Destroy(gameObject);
        else
            _instance = this;
    }
    
#endregion

	public int currentLevel;
	public Brick brickPrefab;
    public Sprite[] sprites;
	public Color[] brickColors;
	
	private int maxRows = 17;
	private int maxColumns = 12;
	private float initialBrickSpawnPositionX = -1.96f;
	private float initialBrickSpawnPositionY = 3.325f;
	private float shiftAmount = 0.365f;
	private GameObject bricksContainer;

	public List<Brick> RemainingBricks { get; set; }
	public List<int[,]> LevelsData { get; set; }
	public int InitialBricksCount { get; set; }

    private void Start()
	{
		this.bricksContainer = new GameObject("BricksContainer");
		this.LevelsData = this.LoadLevelData();
		this.GenerateBricks();
	}

    public void LoadNextLevel()
    {
        this.currentLevel++;

		if(this.currentLevel >= this.LevelsData.Count)
			GameManager.Instance.ShowVictoryPanel();
		else
			this.LoadLevel(this.currentLevel);
    }

    public void LoadLevel(int level)
    {
        this.currentLevel = level;
		this.ClearRemaningBricks();
		this.GenerateBricks();
    }

	public void ClearRemaningBricks()
	{
		foreach(Brick brick in this.RemainingBricks.ToList())
			Destroy(brick.gameObject);
	}

	private void GenerateBricks()
	{
		this.RemainingBricks = new List<Brick>();
		int[,] currentLevelData = this.LevelsData[this.currentLevel];
		float currentSpawnX = initialBrickSpawnPositionX;
		float currentSpawnY = initialBrickSpawnPositionY;
		float zShift = 0;

		for(int row =0; row < this.maxRows; row++)
		{
			for(int column = 0; column < this.maxColumns; column++)
			{
				int brickType = currentLevelData[row, column];

				if(brickType > 0)
				{
					Brick newBrick = Instantiate(brickPrefab, new Vector3(currentSpawnX, currentSpawnY, 0.0f - zShift), Quaternion.identity) as Brick;
					newBrick.Init(bricksContainer.transform, this.sprites[brickType - 1], this.brickColors[brickType], brickType);
				
					this.RemainingBricks.Add(newBrick);
					zShift += 0.0001f;
				}

				currentSpawnX += shiftAmount;

				if(column + 1 == this.maxColumns)
					currentSpawnX = initialBrickSpawnPositionX;
			}

			currentSpawnY -= shiftAmount;
		}

		this.InitialBricksCount = this.RemainingBricks.Count;
		OnLevelLoaded?.Invoke();
	}

	private List<int[,]> LoadLevelData()
	{
		TextAsset text = Resources.Load("levels") as TextAsset;

		string[] rows = text.text.Split(new string[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);

		List<int[,]> levelsData = new List<int[,]>();
		int[,] currentLevel = new int[maxRows, maxColumns];
		int currentRow = 0;

		for(int row = 0; row < rows.Length; row++)
		{
			string line = rows[row];

			if(line.IndexOf("--") == -1)
			{
				string[] bricks = line.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
				for(int column = 0; column < bricks.Length; column++)
				{
					currentLevel[currentRow, column] = int.Parse(bricks[column]);
				}

				currentRow++;
			}
			else
			{
				currentRow = 0;
				levelsData.Add(currentLevel);
				currentLevel = new int[maxRows, maxColumns];
			}
		}

		return levelsData;
	}
}
