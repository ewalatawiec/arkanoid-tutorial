﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{

#region Singleton
    
    private static GameManager _instance;
    public static GameManager Instance => _instance;

    private void Awake()
    {
        if(_instance != null)
        	Destroy(gameObject);
        else
            _instance = this;
    }
    
#endregion

	public int availibleLives = 3;
	public GameObject gameOverPanel;
	public GameObject victoryPanel;

	public static event Action<int> onLiveLost;

	public int Lives { get; set; }
    public bool IsGameStarted { get; set; }

    private void Start()
    {
		Screen.SetResolution(540, 960, false);

		this.Lives = this.availibleLives;
		Ball.onBallDeath += OnBallDeath;
		Brick.onBrickDestruction += OnBrickDestruction;
	}

	private void OnBrickDestruction(Brick obj)
	{
		if(BricksManager.Instance.RemainingBricks.Count <= 0)
		{
			BallsManager.Instance.ResetBalls();
			GameManager.Instance.IsGameStarted = false;
			BricksManager.Instance.LoadNextLevel();
		}
	}

	public void ResartGame()
	{
		SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
	}

	private void OnBallDeath(Ball obj)
	{
		if(BallsManager.Instance.Balls.Count <= 0)
		{
			this.Lives--;

			if(this.Lives < 1)
				 gameOverPanel.SetActive(true);
			else
			{
				onLiveLost?.Invoke(this.Lives);
				BallsManager.Instance.ResetBalls();
				IsGameStarted = false;
				//BricksManager.Instance.LoadLevel(BricksManager.Instance.currentLevel);
			}
		}
	}

    public void ShowVictoryPanel()
    {
        victoryPanel.SetActive(true);
    }

    private void OnDisable()
	{
		Ball.onBallDeath -= OnBallDeath;
	}
}
