﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class BallsManager : MonoBehaviour
{

#region Singleton
    
    private static BallsManager _instance;
    public static BallsManager Instance => _instance;

    private void Awake()
    {
        if(_instance != null)
        	Destroy(gameObject);
        else
            _instance = this;
    }
    
#endregion

	[SerializeField]
	private Ball ball;

	private Ball initialBall;
	private Rigidbody2D initialBallRigidbody;

	public float initialBallSpeed = 250;

    public List<Ball> Balls { get; set; }

	private void Start()
	{
		InitBall();
	}

	private void Update()
	{
		if(!GameManager.Instance.IsGameStarted)
		{
			Vector3 paddlePosition = Paddle.Instance.gameObject.transform.position;
			Vector3 ballPosition = new Vector3(paddlePosition.x, paddlePosition.y + 0.27f, 0);
			initialBall.transform.position = ballPosition;

			if(Input.GetMouseButtonDown(0))
			{
				initialBallRigidbody.isKinematic = false;
				initialBallRigidbody.AddForce(new Vector2(0, initialBallSpeed));
				GameManager.Instance.IsGameStarted = true;
			}	
		}
	}

	public void SpawnBalls(Vector3 position, int count, bool isLightingBall)
	{
		for(int i = 0; i < count; i++)
		{
			Ball spawnedBall = Instantiate(ball, position, Quaternion.identity) as Ball;

			if(isLightingBall)
				spawnedBall.StartLightingBall();

			Rigidbody2D spawnedBallRb = spawnedBall.GetComponent<Rigidbody2D>();

			spawnedBallRb.isKinematic = false;
			spawnedBallRb.AddForce(new Vector2(0, initialBallSpeed));
			
			this.Balls.Add(spawnedBall);
		}
	}

    public void ResetBalls()
    {
        foreach(var ball in this.Balls.ToList())
			Destroy(ball.gameObject);

		InitBall();
    }

    private void InitBall()
	{
		Vector3 paddlePosition = Paddle.Instance.gameObject.transform.position;
		Vector3 startingPosition = new Vector3(paddlePosition.x, paddlePosition.y + 0.27f, 0);

		initialBall = Instantiate(ball, startingPosition, Quaternion.identity);
		initialBallRigidbody = initialBall.GetComponent<Rigidbody2D>();

		initialBallRigidbody.isKinematic = true;

		this.Balls = new List<Ball>()
		{
			initialBall
		};
	}
}
