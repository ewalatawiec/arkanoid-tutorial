﻿public class ShootingPaddle : Collectable
{
    protected override void ApplyEffects()
    {
        Paddle.Instance.StartShooting();
    }
}
