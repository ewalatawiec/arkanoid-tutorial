﻿using System.Collections.Generic;
using UnityEngine;

public class CollectablesManager : MonoBehaviour
{
#region Singleton
    
    private static CollectablesManager _instance;
    public static CollectablesManager Instance => _instance;

    private void Awake()
    {
        if(_instance != null)
        	Destroy(gameObject);
        else
            _instance = this;
    }
    
#endregion

public List<Collectable> avalaibleBuffs;
public List<Collectable> avalaibleDebuffs;

[Range(0, 100)]
public float buffChance;

[Range(0, 100)]
public float debuffChance;
}
