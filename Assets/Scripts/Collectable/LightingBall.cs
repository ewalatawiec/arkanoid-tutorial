﻿public class LightingBall : Collectable
{
	protected override void ApplyEffects()
	{
    	foreach(var ball in BallsManager.Instance.Balls)
			ball.StartLightingBall();
	}
}
