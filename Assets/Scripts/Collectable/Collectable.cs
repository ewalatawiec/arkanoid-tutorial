﻿using UnityEngine;

public abstract class Collectable : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
	{
		if(collision.tag == "Paddle")
			this.ApplyEffects();
		
		if(collision.tag == "Paddle" || collision.tag == "BottomWallCollider")
			Destroy(this.gameObject);
	}

	protected abstract void ApplyEffects();
}
