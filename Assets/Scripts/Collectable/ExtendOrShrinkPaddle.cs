﻿public class ExtendOrShrinkPaddle : Collectable
{
    public float newWidth = 2.5f;

    protected override void ApplyEffects()
    {
        if(Paddle.Instance != null && !Paddle.Instance.PaddleIsTransforming)
            Paddle.Instance.StartWidthAnimation(newWidth);
    }
}
