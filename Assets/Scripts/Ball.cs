﻿using System;
using System.Collections;
using UnityEngine;

public class Ball : MonoBehaviour
{
    public bool isLightingBall;

    public ParticleSystem lightingBallEffect;
    
    public static event Action<Ball> onBallDeath;
    public static event Action<Ball> onLightingBallEnable;
    public static event Action<Ball> onLightingBallDisable;

    
    private float lightingBallDuration = 10;
    private SpriteRenderer spriteRenderer;

    private void Awake()
    {
        this.spriteRenderer = GetComponentInChildren<SpriteRenderer>();
    }

    public void Die()
    {
        onBallDeath?.Invoke(this);
		Destroy(gameObject, 1);
    }

    public void StartLightingBall()
    {
        if(!this.isLightingBall)
        {
            this.isLightingBall = true;
            this.spriteRenderer.enabled = false;
            lightingBallEffect.gameObject.SetActive(true);
            
            StartCoroutine(StopLightingBallAfterTime(this.lightingBallDuration));

            onLightingBallEnable?.Invoke(this);
        }
    }

    private IEnumerator StopLightingBallAfterTime(float seconds)
    {
        yield return new WaitForSeconds(seconds);

        StopFireball();
    }


    private void StopFireball()
    {
        if(this.isLightingBall)
        {
            this.isLightingBall = false;
            this.spriteRenderer.enabled = true;
            
            lightingBallEffect.gameObject.SetActive(false);

            onLightingBallDisable?.Invoke(this);
        }
    }
}
