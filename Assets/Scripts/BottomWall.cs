﻿using UnityEngine;

public class BottomWall : MonoBehaviour
{
    void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "Ball")
		{
			Ball ball = collision.GetComponent<Ball>();
			BallsManager.Instance.Balls.Remove(ball);
			ball.Die();
		}
    }
}
