﻿using System.Collections;
using UnityEngine;

public class Paddle : MonoBehaviour
{

#region Singleton
    
    private static Paddle _instance;
    public static Paddle Instance => _instance;

    public bool PaddleIsTransforming { get; set; }

    private void Awake()
    {
        if(_instance != null)
        	Destroy(gameObject);
        else
            _instance = this;
	}

#endregion

	public float extendOrShrink  = 10;
	public float paddleWidth = 2;
	public float paddleHight = 0.28f;
	public float fireCooldown = 0.5f;
	public float fireColldownLeft = 0;
	public float shootingDuration = 10;
	public GameObject leftMuzzle;
	public GameObject rightMuzzle;
	public Projectile bulletPrefab;

	private float paddleInitialY;
	private float defaultPaddleWidthInPixels = 200;
	private float defaultLeftClamp = 135;
	private float defaultRightClamp = 410;

	private Camera mainCamera;
	private SpriteRenderer spriteRenderer;
	private BoxCollider2D boxCollider;

	public bool PaddleIsShooting { get; set; }

	private void Start()
	{
		mainCamera = FindObjectOfType<Camera>();
		paddleInitialY = this.transform.position.y;
		spriteRenderer = GetComponent<SpriteRenderer>();
		boxCollider = GetComponent<BoxCollider2D>();
	}

	private void Update()
	{
		PaddleMovement();
		UpdateMuzzlePosition();
	}

	public void UpdateMuzzlePosition()
	{
		Vector3 transformPosition = this.transform.position;
		Vector3 size = this.spriteRenderer.size;

		leftMuzzle.transform.position = new Vector3(transformPosition.x - (size.x / 2) + 0.1f, transformPosition.y + 0.2f, transformPosition.z);
		rightMuzzle.transform.position = new Vector3(transformPosition.x + (size.x / 2) - 0.153f, transformPosition.y + 0.2f, transformPosition.z);
	}

	public void StartWidthAnimation(float newWidth)
    {
        StartCoroutine(AnimatePaddleWidth(newWidth));
    }

    public IEnumerator AnimatePaddleWidth(float width)
    {
        this.PaddleIsTransforming = true;
		this.StartCoroutine(ResetPaddleWidthAfterTime(this.extendOrShrink));

		if(width > this.spriteRenderer.size.x)
		{
			float currentWidth = this.spriteRenderer.size.x;
			while(currentWidth < width)
			{
				currentWidth += Time.deltaTime * 2;
				this.spriteRenderer.size = new Vector2(currentWidth, paddleHight);
				boxCollider.size = new Vector2(currentWidth, paddleHight);
				yield return null;
			}
		}
		else
		{
			float currentWidth = this.spriteRenderer.size.x;
			while(currentWidth > width)
			{
				currentWidth -= Time.deltaTime * 2;
				this.spriteRenderer.size = new Vector2(currentWidth, paddleHight);
				boxCollider.size = new Vector2(currentWidth, paddleHight);
				yield return null;
			}
		}

		this.PaddleIsTransforming = false;
    }

	private IEnumerator ResetPaddleWidthAfterTime(float seconds)
	{
		yield return new WaitForSeconds(seconds);
		this.StartWidthAnimation(this.paddleWidth);
	}

    private void PaddleMovement()
	{
		float paddleShift = (defaultPaddleWidthInPixels - ((defaultPaddleWidthInPixels / 2) * this.spriteRenderer.size.x)) / 2;
		float leftClamp = defaultLeftClamp - paddleShift;
		float rightClamp = defaultRightClamp + paddleShift;
		float mousePositionPixels = Mathf.Clamp(Input.mousePosition.x, leftClamp, rightClamp);
		float mousePositionWorldX = mainCamera.ScreenToWorldPoint(new Vector3(mousePositionPixels, 0, 0)).x;

		this.transform.position = new Vector3(mousePositionWorldX, paddleInitialY, 0);
	}

	private void OnCollisionEnter2D(Collision2D collision)
	{
		if(collision.gameObject.tag == "Ball")
		{
			Rigidbody2D ballRigidbody = collision.gameObject.GetComponent<Rigidbody2D>();

			Vector3 transformPosition = this.gameObject.transform.position;
			Vector3 hitPoint = collision.contacts[0].point;
			Vector3 paddleCenter = new Vector3(transformPosition.x, transformPosition.y);

			ballRigidbody.velocity = Vector2.zero;

			float difference = paddleCenter.x - hitPoint.x;

			if(hitPoint.x < paddleCenter.x)
				ballRigidbody.AddForce(new Vector2(-(Mathf.Abs(difference * 200)), BallsManager.Instance.initialBallSpeed));
			else
				ballRigidbody.AddForce(new Vector2((Mathf.Abs(difference * 200)), BallsManager.Instance.initialBallSpeed));
		}
	}

	public void StartShooting()
    {
        if(!this.PaddleIsShooting)
		{
			this.PaddleIsShooting = true;
			StartCoroutine(StartShootingRoutine());
		}
    }

	public IEnumerator StartShootingRoutine()
	{
		float shootingDurationLeft = shootingDuration;

		while(shootingDurationLeft >= 0)
		{
			fireColldownLeft -= Time.deltaTime;
			shootingDurationLeft -= Time.deltaTime;

			if(fireColldownLeft <= 0)
			{
				this.Shoot();
				fireColldownLeft = fireCooldown;
			}

			yield return null;
		}

		this.PaddleIsShooting = false;
		leftMuzzle.SetActive(false);
		rightMuzzle.SetActive(false);
	}

    private void Shoot()
    {
		leftMuzzle.SetActive(false);
		rightMuzzle.SetActive(false);

        leftMuzzle.SetActive(true);
		rightMuzzle.SetActive(true);

		this.SpawnBullet(leftMuzzle);
		this.SpawnBullet(rightMuzzle);
    }

    private void SpawnBullet(GameObject muzzle)
    {
        Vector3 spawnPosition = new Vector3(muzzle.transform.position.x, muzzle.transform.position.y + 0.2f, muzzle.transform.position.z);
		Projectile bullet = Instantiate(bulletPrefab, spawnPosition, Quaternion.identity);
		Rigidbody2D bulletRigidbody = bullet.GetComponent<Rigidbody2D>();
		bulletRigidbody.AddForce(new Vector2(0, 450));
    }
}
