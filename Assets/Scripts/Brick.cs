﻿using System;
using System.Collections.Generic;
using UnityEngine;
using static UnityEngine.ParticleSystem;

public class Brick : MonoBehaviour
{
	public int hitpoints = 1;
	public ParticleSystem destroyEffect;
	public static event Action<Brick> onBrickDestruction;

	private SpriteRenderer spriteRenderer;
	private BoxCollider2D boxCollider;
	

	private void Awake()
	{
		this.spriteRenderer = this.GetComponent<SpriteRenderer>();
		this.boxCollider = this.GetComponent<BoxCollider2D>();
		Ball.onLightingBallEnable += OnLightingBallEnable;
		Ball.onLightingBallDisable += OnLightingBallDisable;
	}

    private void OnLightingBallEnable(Ball obj)
    {
        if(this != null)
			this.boxCollider.isTrigger = true;
    }

	private void OnLightingBallDisable(Ball obj)
    {
        if(this != null)
			this.boxCollider.isTrigger = false;
    }

    private void OnCollisionEnter2D(Collision2D collision)
	{
		bool instantKill = false;
		
		if(collision.collider.tag == "Ball")
		{
			Ball ball = collision.gameObject.GetComponent<Ball>();
			instantKill = ball.isLightingBall;
		}

		if(collision.collider.tag == "Ball" || collision.collider.tag == "Projectile")
			this.TakeDamage(instantKill);
	}

	private void OnTriggerEnter2D(Collider2D collision)
	{
		bool instantKill = false;
		if(collision.tag == "Ball")
		{
			Ball ball = collision.gameObject.GetComponent<Ball>();
			instantKill = ball.isLightingBall;
		}

		if(collision.tag == "Ball" || collision.tag == "Projectile")
			this.TakeDamage(instantKill);
	}

	private void TakeDamage(bool instantKill)
	{
		this.hitpoints--;

		if(this.hitpoints <= 0 || instantKill)
		{
			BricksManager.Instance.RemainingBricks.Remove(this);
			onBrickDestruction?.Invoke(this);
			OnBrickDestroy();

			SpawnDestroyEffect();
			Destroy(this.gameObject);
		}
		else
			this.spriteRenderer.sprite = BricksManager.Instance.sprites[this.hitpoints - 1];
	}

	private void OnBrickDestroy()
	{
		float buffSpawnChance = UnityEngine.Random.Range(0f, 100f);
		float debuffSpawnChance = UnityEngine.Random.Range(0f, 100f);
		bool alreadySpawned = false;

		if(buffSpawnChance <= CollectablesManager.Instance.buffChance)
		{
			alreadySpawned = true;
			Collectable newBuff = this.SpawnCollectable(true);
		}

		if(debuffSpawnChance <= CollectablesManager.Instance.debuffChance && !alreadySpawned)
		{
			Collectable newDebuff = this.SpawnCollectable(false);
		}
		
	}

    private Collectable SpawnCollectable(bool isBuff)
    {
        List<Collectable> collection;

		if(isBuff)
			collection = CollectablesManager.Instance.avalaibleBuffs;
		else
			collection = CollectablesManager.Instance.avalaibleDebuffs;
		
		int buffIndex = UnityEngine.Random.Range(0, collection.Count);

		Collectable prefab = collection[buffIndex];
		Collectable newCollectable = Instantiate(prefab, this.transform.position, Quaternion.identity) as Collectable;

		return newCollectable;
    }

    private void SpawnDestroyEffect()
	{
		Vector3 brickPosition = gameObject.transform.position;
		Vector3 spawnPosition = new Vector3(brickPosition.x, brickPosition.y, brickPosition.z = 0.2f);

		GameObject effect = Instantiate(destroyEffect.gameObject, spawnPosition, Quaternion.identity);

		MainModule mainModule = effect.GetComponent<ParticleSystem>().main;
		mainModule.startColor = this.spriteRenderer.color;
		
		Destroy(effect, destroyEffect.main.startLifetime.constant);
	}

	public void Init(Transform containerTransform, Sprite sprite, Color color, int hitpoints)
	{
		this.transform.SetParent(containerTransform);
		this.spriteRenderer.sprite = sprite;
		this.spriteRenderer.color = color;
		this.hitpoints = hitpoints;
	}

	private void OnDisable()
	{
		Ball.onLightingBallEnable -= OnLightingBallEnable;
		Ball.onLightingBallDisable -= OnLightingBallDisable;
	}
}
